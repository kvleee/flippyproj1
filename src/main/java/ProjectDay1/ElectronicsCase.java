package ProjectDay1;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

public class ElectronicsCase {

@Test
	public void electronicsPage() throws InterruptedException 
	{
		// TODO Auto-generated method stub

		System.setProperty("webdriver.chrome.driver", "C:\\TestLeaf\\eclipse-workspace1\\FlipkartShopping\\drivers\\chromedriver.exe");

		ChromeDriver driver=new ChromeDriver();

		driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.get("https://www.flipkart.com/");
		//driver.switchTo().alert().dismiss();
		driver.getKeyboard().sendKeys(Keys.ESCAPE);
		Actions builder =new Actions(driver);
		builder.moveToElement(driver.findElement(By.xpath("//span[text()='Electronics']"))).perform();
		// driver.findElement(By.xpath("//span[text()='Electronics']")).click();
		driver.findElementByLinkText("Mi").click();

		WebDriverWait wa = new WebDriverWait(driver,20);
		wa.until(ExpectedConditions.titleContains("Mi"));

		String title= driver.getTitle();
		System.out.println(title);
		if(title.contains("Mi"))
		{
			System.out.println("Title of the Page is: " +title);
		}
		else 
		{
			System.out.println("Title is not inteneded one");
		}

		driver.findElementByXPath("//div[text()=\"Newest First\"]").click();

		List<WebElement> bn = driver.findElementsByClassName("_3wU53n");
		List<String> ls = new ArrayList<>();
		for (WebElement eachBn : bn) {
			ls.add(eachBn.getText());
			Thread.sleep(100);
			}
		
		
		
		List<WebElement> p = driver.findElementsByXPath("//div[@class='_1vC4OE _2rQ-NK']"); //("//div[@class=_1vC4OE _2rQ-NK]");
		
		WebDriverWait wa1 = new WebDriverWait(driver,20);
		wa1.until(ExpectedConditions.visibilityOfAllElements(p));
		
		List<String> ls1 = new ArrayList<>();
		for (WebElement eachp : p) {
		ls1.add(eachp.getText());		
			Thread.sleep(100);
			}
		
		for (int i = 0; i <bn.size()-1; i++) {
			
		//	String k =(ls1.get(i)).replaceAll("//D", "");
			System.out.println(ls.get(i)+"---->"+ls1.get(i));
		}
		
		
		
	}

}
